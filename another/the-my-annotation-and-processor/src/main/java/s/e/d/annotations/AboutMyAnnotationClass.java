package s.e.d.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AboutMyAnnotationClass {
    int value() default 777;
    String someName() default "The SHN[1603 12]";
}
