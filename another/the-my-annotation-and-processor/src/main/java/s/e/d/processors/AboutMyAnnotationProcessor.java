package s.e.d.processors;

import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Date;
import java.util.Set;

@SupportedAnnotationTypes("s.e.d.annotations.AboutMyAnnotation")
@SupportedSourceVersion(SourceVersion.RELEASE_9)
@AutoService(Processor.class)
public class AboutMyAnnotationProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        System.out.println("AboutMyAnnotationProcessor process 1435 " + new Date());
        System.out.println("set -> " + set);
        System.out.println("rEnv -> " + roundEnvironment);
        return false;
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        System.out.println("AboutMyAnnotationProcessor init " + new Date());
        System.out.println("prEnv -> " + processingEnv);
    }
}