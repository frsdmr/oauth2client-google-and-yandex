package s.e.d.processors;

import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Date;
import java.util.Set;

@SupportedAnnotationTypes("s.e.d.annotations.AboutMyAnnotationClass")
@SupportedSourceVersion(SourceVersion.RELEASE_9)
@AutoService(Processor.class)
public class AboutMyAnnotationClassProcessor  extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        System.out.println("AboutMyAnnotationClassProcessor process 1543 (RT (not)) " + new Date());
        System.out.println("annotations -> " + annotations);
        System.out.println("roundEnv -> " + roundEnv);
        return false;
    }
}
