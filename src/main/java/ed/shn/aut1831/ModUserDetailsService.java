package ed.shn.aut1831;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ModUserDetailsService implements UserDetailsService {
    @Override
    public ModUserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        // Encoding password's
        //PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        //PasswordEncoder encoder = new BCryptPasswordEncoder();
        //PasswordEncoder encoderS = new StandardPasswordEncoder();
        //PasswordEncoder encoderA = new Argon2PasswordEncoder();
        //PasswordEncoder encoderP = new Pbkdf2PasswordEncoder();
        //PasswordEncoder encoderR = new SCryptPasswordEncoder();
        //String xx = "{bcrypt}" + encoder.encode("yyy");
        //String xxs = "{sha256}" + encoderS.encode("yyy");
        ////String xxa = "{argon2}"+encoderA.encode("yyy");       // не энкодится почему то...
        //String xxp = "{pbkdf2}" + encoderP.encode("yyy");
        ////String xxr = "{scrypt}"+encoderR.encode("yyy");       // не энкодится почему то...

        // выдергивание из базы информации ( пароль(зашифрованый) и роли ) согласно предоставленного юзернейма
        // заполнение этими данными (pass, roles) User-a? и потом спринг проверяет соответствие введенной информации
        // (что пришло из формы браузера)
        // тому, что указано здесь (дальше спринговая проверка где то во внутрях спринга, и если совпадение, то isAuthorised -> true)
        UserDetails user = User.builder()
                .username("yyy")        // userName
                //.password("{noop}yyy")  // password from storage (next string's - "yyy" too)
                //.password("{sha256}08c803c36cc4b7ae89fd1cd3b763224752ffd2bc15940d19e4c11fd5e73ecaf3ddb28c2bb2301973")
                //.password("{pbkdf2}7d3627b947d80738e45827d569c100b6e75382e98a823e91429bdad36b974c116aed2f0f4105aa49")
                .password("{bcrypt}$2a$10$uUbOyrtZsD/rHZZYbWBF/.EByRVmL09UjPSL3YUF6kDIqk1UoWn5i")
                .roles("USER", "SOMEUSER")          // roles from storage
                .build();

        ModUserDetails modUserDetails = new ModUserDetails(user.getUsername(), user.getPassword(), user.getAuthorities());
        modUserDetails.setShn(" SHN HERE LOGIN_FORM  ModUserDetailsService " + new Date());

        return modUserDetails;
    }
}
