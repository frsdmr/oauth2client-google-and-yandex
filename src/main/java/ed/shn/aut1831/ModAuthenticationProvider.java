package ed.shn.aut1831;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

// used when "loginForm" auth
@Slf4j
@Service
public class ModAuthenticationProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        log.debug(" ModAuthenticationProvider (deb) ");
        String nameFromForm = authentication.getName();
        String passFromForm = authentication.getCredentials().toString();

        // Encoding password's
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

        // userFromDB = dao.findByLogin(nameFromForm);
        // check for name (exist)  else  throw new BadCredentialsException("Unknown user "+nameFromForm);
        if (!"xxx".equals(nameFromForm)) throw new BadCredentialsException("Unknown user " + nameFromForm);

        // check for password (eq) else  throw new BadCredentialsException("Bad password");
        if (!encoder.matches(passFromForm, "{bcrypt}$2a$10$VLTJ5eKS6IRLQJD79.7mUeYXO4wkAZawI5wo5V2Dh8yGdL2sHl9BK"))
        // if (!encoder.matches(passFromForm, "{noop}xxx")) // same (xxx - without crypto) - encoder is working too
            throw new BadCredentialsException("Bad password");
        // permission from db
        String[] rolesFromDb = new String[]{"USER", "SOMEUSER", "ADMIN"};

        UserDetails formLoginUserDetails = User.builder()
                .username(nameFromForm)        // userName ()
                .password(passFromForm)        // password from storage (like "{noop}xxx" or empty (it will be cleaned by spring automatically ))
                .roles(rolesFromDb)            // roles from storage
                .build();

        ModUserDetails modUserDetails = new ModUserDetails(formLoginUserDetails.getUsername(), formLoginUserDetails.getPassword(), formLoginUserDetails.getAuthorities());
        modUserDetails.setShn(" SHN HERE LOGIN_FORM ModAuthenticationProvider " + new Date());

        UsernamePasswordAuthenticationToken passAuthToken = new UsernamePasswordAuthenticationToken(
                modUserDetails, modUserDetails.getPassword(), modUserDetails.getAuthorities());

        return passAuthToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }


}
