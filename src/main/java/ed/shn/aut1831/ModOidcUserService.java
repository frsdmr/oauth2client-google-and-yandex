package ed.shn.aut1831;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * экстракт и модификация авторити и т.п.
 * Гугловый обработчик
 */

@Slf4j
@Service
public class ModOidcUserService extends OidcUserService {

    @Autowired
    ModOAuth2UserService modOAuth2UserService;

    @Override
    public ModDefaultOidcUser loadUser(OidcUserRequest oidcUserRequest) throws OAuth2AuthenticationException {

        log.info("SHN OID");

        OidcUser oidcUser = super.loadUser(oidcUserRequest);

        OidcIdToken origIdToken = oidcUser.getIdToken();

        // set Authorities ( roles )
        Set<GrantedAuthority> modAuthorities = new LinkedHashSet<>();
        oidcUser.getAuthorities().stream().forEach(a -> modAuthorities.add(a));

        // можно сделать проверки и добавить нужные авторити
        modAuthorities.add(new OidcUserAuthority("ROLE_SOMEUSER", origIdToken, oidcUser.getUserInfo()));

        // для модификации _аттрибутов_ необходимо  перегенерировать IdToken и использовать его в конструкторе
        // вместо oidcUser.getAttributes().put("some", "here_now_Oidc") т.к. пичаль -  unmodifiableMap и нет конструктора
        // с атрибутами ( есть только с IdToken ) - new ModDefaultOidcUser(modAuthorities, newIdToken)

        // set additional Attributes ( claim )
        Map<String, Object> newClaims = new HashMap<>();
        newClaims.put("some", "here_now_Oidc");
        origIdToken.getClaims().entrySet().stream().forEach(c -> newClaims.put(c.getKey(), c.getValue()));

        // newIdToken
        OidcIdToken newIdToken = new OidcIdToken(origIdToken.getTokenValue(), origIdToken.getIssuedAt(), origIdToken.getExpiresAt(), newClaims);

        // modDefaultOidcUser
        ModDefaultOidcUser modDefaultOidcUser = new ModDefaultOidcUser(modAuthorities, newIdToken);

        modDefaultOidcUser.setShn("  SHN HERE OID GOOGLE  [" + new Date() + "]");

        return modDefaultOidcUser;
    }


}
