package ed.shn.aut1831;

import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;

import java.util.Collection;
import java.util.Date;

@ToString(callSuper = true)
public class ModDefaultOidcUser extends DefaultOidcUser implements ModPrincipal{

    String shn = "the shn ModDefaultOidcUser  " + new Date();

    public ModDefaultOidcUser(Collection<? extends GrantedAuthority> authorities, OidcIdToken idToken) {
        super(authorities, idToken);
    }

    @Override
    public String getShn() {
        return shn;
    }

    @Override
    public String setShn(String newValueShn) {
        shn = newValueShn;
        return shn;
    }
}
