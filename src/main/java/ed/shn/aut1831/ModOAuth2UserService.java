package ed.shn.aut1831;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.stereotype.Service;

import java.util.*;


/*
 * экстракт и модификация авторити и т.п.
 * Yandex, GitHub обработчик
 **/
@Slf4j
@Service
public class ModOAuth2UserService extends DefaultOAuth2UserService {

    @Override
    public ModDefaultOAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {

        log.info("SHN OAuth2");
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

        // здесь можно что то проверить (oAuth2User) и добавить аттрибуты, роли в авторити

        // modified Attributes
        Map<String, Object> modifiedAttribs = new HashMap<>();
        modifiedAttribs.put("some", "here_now_OAuth2");
        oAuth2User.getAttributes().entrySet().stream().forEach(attr -> modifiedAttribs.put(attr.getKey(), attr.getValue()));

        // modified Authorities
        Set<GrantedAuthority> modifiedAuthorities = new LinkedHashSet<>();
        modifiedAuthorities.add(new OAuth2UserAuthority("ROLE_ADMIN", modifiedAttribs));
        oAuth2User.getAuthorities().stream().forEach(a -> modifiedAuthorities.add(a));

        ModDefaultOAuth2User modDefaultOAuth2User = new ModDefaultOAuth2User(modifiedAuthorities, modifiedAttribs,
                oAuth2UserRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint()
                        .getUserNameAttributeName());

        modDefaultOAuth2User.setShn("  SHN HERE OAuth2 Yandex  [" + new Date() + "]");

        return modDefaultOAuth2User;
    }
}
