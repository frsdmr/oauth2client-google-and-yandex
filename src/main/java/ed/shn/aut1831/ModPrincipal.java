package ed.shn.aut1831;

public interface ModPrincipal {
    String getShn();
    String setShn(String newValueShn);
}
