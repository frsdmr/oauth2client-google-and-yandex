package ed.shn.aut1831;

import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Date;

@ToString(callSuper = true)
public class ModUserDetails extends User implements ModPrincipal{

    public String shn = "the shn ModUserDetails  " + new Date();

    public ModUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public ModUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    @Override
    public String getShn() {
        return shn;
    }

    @Override
    public String setShn(String newValueShn) {
        this.shn = newValueShn;
        return shn;
    }
}
