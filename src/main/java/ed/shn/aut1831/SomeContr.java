package ed.shn.aut1831;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


@Data
@Slf4j
@RestController
public class SomeContr {

    @Autowired
    OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository;

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @RequestMapping("/")
    public String indTheOnlyRoot(HttpServletRequest r) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        theLog(r, auth);

        //ModPrincipal princpl = (ModPrincipal) auth.getPrincipal();

        return "XXX Only Root [" + auth.getPrincipal() + "][" + this + "] ( " + r.getRequestURL().toString() + " ) " + new Date();
    }

    @RequestMapping("/**")
    public String indThRoot(HttpServletRequest r) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        OAuth2AuthorizedClient xxx = oAuth2AuthorizedClientRepository.loadAuthorizedClient(((OAuth2AuthenticationToken) auth).getAuthorizedClientRegistrationId(), auth, r);
//        OAuth2AuthorizedClient yyy = authorizedClientService.loadAuthorizedClient(((OAuth2AuthenticationToken) auth).getAuthorizedClientRegistrationId(), auth.getName());

        theLog(r, auth);

        return "XXX  Root [" + this + "] ( " + r.getRequestURL().toString() + " ) " + new Date();
    }

    @RequestMapping("/au")
    public String indThAU(HttpServletRequest r) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        theLog(r, auth);

        return "XXX [" + this + "] " + new Date();
    }

    private void theLog(HttpServletRequest r, Authentication auth) {
        log.info(" root [" + this + "] (" + r.getRequestURL().toString() + ") " + new Date());
        log.info("\n{}", auth);
        log.info("");
    }

    @RequestMapping("/cAccessDeniedPage")
    public String cAccessDeniedPage(HttpServletRequest r) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return "THE cAccDenPage   [" + this + "] " + new Date() + " [" + auth + "]";
    }

    @RequestMapping("/authErr")
    public String authErr(HttpServletRequest r) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return "THE ERROR   [" + this + "] " + new Date() + " [" + auth + "]";
    }

    @RequestMapping("/auth")
    public String auth(HttpServletRequest r) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

//        return "THE LOGIN XXX [" + this + "] " + new Date() + " [" + auth + "]";
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n" +
                "    <meta name=\"description\" content=\"\">\n" +
                "    <meta name=\"author\" content=\"\">\n" +
                "    <title>Please sign in CUSTOM</title>\n" +
                "    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M\" crossorigin=\"anonymous\">\n" +
                "    <link href=\"https://getbootstrap.com/docs/4.0/examples/signin/signin.css\" rel=\"stylesheet\" crossorigin=\"anonymous\"/>\n" +
                "  </head>\n" +
                "  <body>\n" +
                "     <div class=\"container\">\n" +
                "      <form class=\"form-signin\" method=\"post\" action=\"/login\">\n" +
                "        <h2 class=\"form-signin-heading\">Please sign in CUSTOM</h2>\n" +
                "        <p>\n" +
                "          <label for=\"username\" class=\"sr-only\">Username</label>\n" +
                "          <input type=\"text\" id=\"username\" name=\"username\" class=\"form-control\" placeholder=\"Username\" required autofocus>\n" +
                "        </p>\n" +
                "        <p>\n" +
                "          <label for=\"password\" class=\"sr-only\">Password</label>\n" +
                "          <input type=\"password\" id=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" required>\n" +
                "        </p>\n" +
                "<p><input type='checkbox' name='remember-me'/> Remember me on this computer.</p>\n" +
                "        <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Sign in</button>\n" +
                "      </form>\n" +
                "<h2 class=\"form-signin-heading\">Login with OAuth 2.0 CUSTOM</h2><table class=\"table table-striped\">\n" +
                " <tr><td><a href=\"/oauth2/authorization/github\">GitHub</a></td></tr>\n" +
                " <tr><td><a href=\"/oauth2/authorization/facebook\">Facebook</a></td></tr>\n" +
                " <tr><td><a href=\"/oauth2/authorization/google\">Google</a></td></tr>\n" +
                " <tr><td><a href=\"/oauth2/authorization/yandex\">Yandex</a></td></tr>\n" +
                "</table>\n" +
                "</div>\n" +
                "</body></html>";
    }
}
