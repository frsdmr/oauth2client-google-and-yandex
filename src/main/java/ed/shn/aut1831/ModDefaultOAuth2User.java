package ed.shn.aut1831;

import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;

import java.util.Collection;
import java.util.Date;
import java.util.Map;



@ToString(callSuper = true)
public class ModDefaultOAuth2User extends DefaultOAuth2User implements ModPrincipal {

    public String shn = "the shn ModDefaultOAuth2User  " + new Date(); // сквозной auth ID можно во время проверки пользователя сервисом положить например сюда

    public ModDefaultOAuth2User(Collection<? extends GrantedAuthority> authorities, Map<String, Object> attributes, String nameAttributeKey) {
        super(authorities, attributes, nameAttributeKey);
    }

    @Override
    public String getShn() {
        return shn;
    }

    @Override
    public String setShn(String newValueShn) {
        shn = newValueShn;
        return shn;
    }
}
