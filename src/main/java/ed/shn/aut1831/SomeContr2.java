package ed.shn.aut1831;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@RequestMapping("/some")
@Controller
public class SomeContr2 {

    @Autowired
    private OAuth2AuthorizedClientService clientService;

    @GetMapping("/xx")
    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PreAuthorize("hasRole('ADMIN')")
    public String xxMap(Model model) {
        model.addAttribute("someInfo","The some info here");
        return "th_xxfile";            // <- return template file with filled model
    }


    // - - - - - - - - - - - - - for this can use @RestController  ( instead @Controller &  @ResponseBody ) - - - - - - - - -
    // - ЭТО не совсем REST
    @GetMapping(value = "/xx1")
    @ResponseBody
    // возвращается не объект, а фактически строка полученная из объекта
    public String xx1Map() {
        ModPrincipal principal = (ModPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        SecurityContext securityContext = SecurityContextHolder.getContext();
        OAuth2AuthenticationToken oauth2Token = (OAuth2AuthenticationToken)
                securityContext.getAuthentication();

        OAuth2AuthorizedClient client = clientService
                .loadAuthorizedClient(oauth2Token.getAuthorizedClientRegistrationId(),
                        oauth2Token.getName());

        String refreshToken = client.getRefreshToken().getTokenValue();
        String accessToken = client.getAccessToken().getTokenValue();

        return principal.toString();  // return string
    }

    // - ЭТО   УЖЕ    REST
    @GetMapping(value = "/xx2"/*, produces = "application/xml"*/)
    @ResponseBody
    // возвращается настоящий JSON (объект)
    public ModPrincipal xx2Map() {
        ModPrincipal principal = (ModPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return principal;  // return as json
    }

    @GetMapping(value = "/xx3"/*, produces = "application/xml"*/)
    @ResponseBody
    // возвращается настоящий JSON (объект) с кастомными хидерами и кастомным статусом
    public ResponseEntity xx3() {
        ModPrincipal principal = (ModPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Custom-Header-someHere", "some here value");

        ResponseEntity<ModPrincipal> re = new ResponseEntity<ModPrincipal>(principal, headers, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        return re;  // return as custom
    }

}
