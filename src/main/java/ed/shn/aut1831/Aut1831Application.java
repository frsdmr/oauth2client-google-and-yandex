package ed.shn.aut1831;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
//import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
//@EnableHystrixDashboard
//@EnableCircuitBreaker
public class Aut1831Application {

	public static void main(String[] args) {
		SpringApplication.run(Aut1831Application.class, args);
	}

}
