package ed.shn.aut1831;

import org.springframework.stereotype.Component;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;


/*
 * магия... переложено в *компонент
 *
 *              ... этого достаточно чтобы работало
 *                      (при комментировании компонента естественно не работает,
 *                          используется некий дефолтный шаблонизатор)
 * */



@Component
public class ModThymeleafViewResolver extends ThymeleafViewResolver {

    public ModThymeleafViewResolver() {
        super();

        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();

        templateResolver.setPrefix("/mytemplates/"); // здесь префикс устанавливается с ведущим (первым) слешем без "classpass:" (как в app.prop)
        templateResolver.setCacheable(false);
        templateResolver.setSuffix(".htmlth");
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCharacterEncoding("UTF-8");


        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        templateEngine.setEnableSpringELCompiler(true);

        ThymeleafViewResolver viewResolver = this; //new ThymeleafViewResolver();

        viewResolver.setTemplateEngine(templateEngine);
        viewResolver.setCharacterEncoding("UTF-8");
        viewResolver.setOrder(0);

        // Important!!
        // th_page1.html, th_page2.html, ...
        viewResolver.setViewNames(new String[]{"th_*"});

    }
}
