package ed.shn.aut1831;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

@Slf4j
@Data
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true) // для функционирования @PreAuthorize
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    // для собственной пост обработки OAuth2/OID аутенификации - модификации authority / сохранения локальных данных о пользователях /etc
    @Autowired
    ModOAuth2UserService modOAuth2UserService;

    @Autowired
    ModOidcUserService modOidcUserService;


    // для обработки FormLogin аутенификации (можно использовать что то одно (или переопределять сервис, или провайдер))
    @Autowired
    ModUserDetailsService modUserDetailsService;

    @Autowired
    ModAuthenticationProvider modAuthenticationProvider;

    @Override
    public void configure(AuthenticationManagerBuilder builder) throws Exception {

        //  при использовании стандартных мех-мов спринг кастомность (новые поля в принципале) использовать не получится
        //  (ModPrincipal) auth.getPrincipal() - приведение типов (для исп-я каст. геттеров) работать не будет...
        //  (remember me работает только с inMemoryAuthentication и переопределенным сервисом(modUserDetailsService),
        //  а с переопределенным провайдером - не работает)
        //
        builder.inMemoryAuthentication().withUser("some1").password("{noop}some1").roles("USER", "ADMIN", "SOMEUSER");

        // выдергивание инфы на основании имени (userName) и передается спрингу для дальнейшей обработки
        builder.userDetailsService(modUserDetailsService);

        // полная замена провайдера. надо самостоятельно проверять соответствие имени и пароля и на основании полученых (Authentication),
        // и при успехе заполнять (UsernamePasswordAuthenticationToken) ()
        builder.authenticationProvider(modAuthenticationProvider);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests(a -> a
                .antMatchers("/au", "/yandex_4f2d534486f17935.html", "/theSomeAbout", "/gm", "/js/lib/*", "/geodata/*", "/favicon.ico").permitAll()
                .antMatchers("/gru0").access("hasAnyRole('USER','ADMIN')")
                .antMatchers("/gru1").hasAnyRole("USER", "ADMIN")
                .antMatchers("/gru2").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                .antMatchers("/gra", "/actuator", "/actuator/**").hasRole("ADMIN")
                .antMatchers("/some/*").hasAnyAuthority("ROLE_SOMEUSER", "ROLE_ADMIN")  // непосредственно в контроллере можно ограничить
                .antMatchers("/user/**", "/user**").hasRole("USER")                                 // @PreAuthorize("hasAuthority('ROLE_ADMIN')")
                .antMatchers("/").authenticated()                                                   // доступ для определенных методов
                //.anyRequest().authenticated()     // любой реквест отработает при наличии аутентификации
                // (все что не перечислено выше - будет отдаваться
                // любому залогиненому пользователю)
                // у OAuth2 это включено - в отличии от остальных авторизаций.
                // все дефолтное/неразрешенное надо блокировать в явном виде
                // например требовать левую роль
                .antMatchers("/**").hasRole("kjhdvkjdzgkjdasgfl")
        )

                //.oauth2Login(Customizer.withDefaults()) // таким образом гугловая(дефолтные) + яндекс(кастомные) авторизации отрабатывают
                //.oauth2Login() // и таким образом тоже отрабатывают

                .exceptionHandling(eh -> {
                    eh.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/auth"));  // кастомная страница авторизации
                    eh.accessDeniedPage("/cAccessDeniedPage");
                })
                .oauth2Login(oauth2Login -> oauth2Login
                        //.tokenEndpoint(tokenEndpointConfig -> tokenEndpointConfig.accessTokenResponseClient(modDefaultAuthorizationCodeTokenResponseClient))
                        .failureUrl("/authErr")
                        .userInfoEndpoint(userInfoEndpoint -> userInfoEndpoint
                                .userService(modOAuth2UserService)
                                .oidcUserService(modOidcUserService)))

                .formLogin()  // additional FormLogin authentication
                .failureHandler((httpServletRequest, httpServletResponse, e) -> httpServletResponse.getWriter().write("THE AUTH failure"))  // кастомная страница ошибки авторизации

                .and().logout().logoutSuccessUrl("/auth").permitAll()  // на кастомную страницу авторизации

                //.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // а с этим (при кастомном AuthenticationProvider) даже авторизации не проходит
                .and().rememberMe().key("777")  //  а вот это(запоминание) при использовании (inMemoryAuthentication)) не работает / работает с (UserDetailsService)
                .and().csrf().disable()

        ;


//
//        http                                                                // >>>>>   for password block auth start
//                .formLogin()
//                .loginPage("/login")                                        // to auth form
//                .permitAll();                                               // <<<<<<  for password block auth finish

    }

}


